Kitacon 2015 Project
====================

This is the git project page for Kitacon 2015, please refer to the [issues tracker](https://bitbucket.org/linuxpenguin/kitacon-2015/issues) or [wiki](https://bitbucket.org/linuxpenguin/kitacon-2015/wiki) for more information.