<?php

########### Frontend Routes ###########

Route::get( '/', function ()
{

});


############ Admin Routes #############

Route::get('backend/login', [
    'as'     =>  'backend.auth.login',
    'before' =>  'guest',
    'uses'   =>  'BackendAuthController@getLogin',
]);

Route::post('backend/login', [
    'as'     =>  'backend.auth.login',
    'before' =>  'guest',
    'uses'   =>  'BackendAuthController@postLogin',
]);

Route::group(['prefix' => 'backend', 'before' => 'admin_auth'], function()
{
    Route::get('/', [
        'as'    =>  'backend.dashboard.index',
        'uses'  =>  'BackendDashboardController@getIndex',
    ]);

    Route::get('logout', [
        'as'    =>  'backend.auth.logout',
        'uses'  =>  'BackendAuthController@getLogout',
    ]);

    Route::group(['prefix' => 'articles'], function()
    {
        Route::get('/', [
            'as'    =>  'backend.articles.index',
            'uses'  =>  'BackendArticlesController@getIndex',
        ]);

        Route::get('{id}/edit', [
            'as'    =>  'backend.articles.edit',
            'uses'  =>  'BackendArticlesController@edit',
        ]);

        Route::put('{id}', [
            'as'    =>  'backend.articles.update',
            'uses'  =>  'BackendArticlesController@update',
        ]);

        Route::get('create', [
            'as'    =>  'backend.articles.create',
            'uses'  =>  'BackendArticlesController@create',
        ]);

        Route::post('{id}', [
            'as'    =>  'backend.articles.store',
            'uses'  =>  'BackendArticlesController@store',
        ]);

        Route::get('{id}/delete', [
            'as'    =>  'backend.articles.destroy',
            'uses'  =>  'BackendArticlesController@delete',
        ]);
    });

    Route::group(['prefix' => 'cms'], function()
    {
        Route::get('/', [
            'as'    =>  'backend.cms.index',
            'uses'  =>  'BackendPagesController@getIndex',
        ]);

        Route::get('{id}/edit', [
            'as'    =>  'backend.cms.edit',
            'uses'  =>  'BackendPagesController@edit',
        ]);

        Route::put('{id}', [
            'as'    =>  'backend.cms.update',
            'uses'  =>  'BackendPagesController@update',
        ]);

        Route::get('create', [
            'as'    =>  'backend.cms.create',
            'uses'  =>  'BackendPagesController@create',
        ]);

        Route::post('{id}', [
            'as'    =>  'backend.cms.store',
            'uses'  =>  'BackendPagesController@store',
        ]);

        Route::get('{id}/delete', [
            'as'    =>  'backend.cms.destroy',
            'uses'  =>  'BackendPagesController@delete',
        ]);
    });

    Route::group(['prefix' => 'account'], function()
    {
        Route::get('/', [
            'as'    =>  'backend.account.index',
            'uses'  =>  'BackendAccountController@getIndex',
        ]);
    });

    Route::group(['prefix' => 'users'], function()
    {
        Route::get('/', [
            'as'    =>  'backend.users.index',
            'uses'  =>  'BackendUsersController@getIndex',
        ]);

        Route::get('{id}/edit', [
            'as'    =>  'backend.users.edit',
            'uses'  =>  'BackendUsersController@edit',
        ]);

        Route::put('{id}', [
            'as'    =>  'backend.users.update',
            'uses'  =>  'BackendUsersController@update',
        ]);

        Route::get('create', [
            'as'    =>  'backend.users.create',
            'uses'  =>  'BackendUsersController@create',
        ]);

        Route::post('{id}', [
            'as'    =>  'backend.users.store',
            'uses'  =>  'BackendUsersController@store',
        ]);

        Route::get('{id}/delete', [
            'as'    =>  'backend.users.destroy',
            'uses'  =>  'BackendUsersController@delete',
        ]);
    });

    Route::group(['prefix' => 'search'], function()
    {
        Route::get('/', [
            'as'    =>  'backend.search.index',
            'uses'  =>  'BackendSearchController@getIndex',
        ]);
    });
});