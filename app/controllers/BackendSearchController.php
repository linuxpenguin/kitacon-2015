<?php

class BackendSearchController extends BaseController {

    protected $layout = 'backend.template';

    protected $breadcrumbs;

    public function __construct(Breadcrumb $breadcrumb)
    {
        $this->breadcrumbs = $breadcrumb;
        $this->breadcrumbs->addLink('Dashboard', route('backend.dashboard.index'));
        $this->breadcrumbs->addLink('Search', route('backend.search.index'));
        $this->breadcrumbs->addLink('Results', route('backend.search.index'));
    }

    public function getIndex()
    {
        $this->layout->title = 'Kitacon Committee - Search Results';
        $this->layout->content = View::make('backend.search.index');

        if(Input::get('q'))
        {
            // Find results in articles
            $objArticles = Article::where('title', 'LIKE', '%'.Input::get('q').'%')
                ->orWhere('content', 'LIKE', '%'.Input::get('q').'%')
                ->get();

            // Find results in cms pages
            $objPages = Page::where('title', 'LIKE', '%'.Input::get('q').'%')
                ->orWhere('content', 'LIKE', '%'.Input::get('q').'%')
                ->get();


            // Find results in user accounts
            $objUsers = User::where('email', 'LIKE', '%'.Input::get('q').'%')
                ->orWhere('first_name', 'LIKE', '%'.Input::get('q').'%')
                ->orWhere('last_name', 'LIKE', '%'.Input::get('q').'%')
                ->get();

            $this->layout->content->intResults = $objArticles->count() + $objPages->count() + $objUsers->count();

            if($this->layout->content->intResults > 0)
            {
                $this->layout->content->objArticles = $objArticles;
                $this->layout->content->objPages    = $objPages;
                $this->layout->content->objUsers    = $objUsers;
            }

        }
        else
        {
            $this->layout->content->intResults = 0;
        }

        $this->layout->arrBreadcrumbs = $this->breadcrumbs->getBreadCrumbs();
    }

    /*public function getResults($strQuery)
    {
        $this->layout->title = 'Kitacon Committee - Search Results';
        $this->layout->content = View::make('backend.search.results');

        $this->breadcrumbs->addLink('Results', route('backend.results.index'));
        $this->layout->arrBreadcrumbs = $this->breadcrumbs->getBreadCrumbs();
    }*/

}