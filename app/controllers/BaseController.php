<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);

            $objBreadcrumbs = new Breadcrumb;
            $objBreadcrumbs->addLink('Dashboard', route('backend.dashboard.index'));
            $this->layout->arrBreadcrumbs = $objBreadcrumbs->getBreadCrumbs();
		}
	}

}
