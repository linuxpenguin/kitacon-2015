<?php

class BackendAuthController extends BaseController {

    protected $layout = 'backend.template';

    public function getLogin()
    {
        $this->layout->title = 'Kitacon Committee - Login';
        $this->layout->content = View::make('backend.auth.login');
    }

    public function postLogin()
    {
        if(Auth::attempt(['email' => Input::get('email'), 'password' => Input::get('password'), 'admin' => true]))
        {
            return Redirect::intended(route('backend.dashboard.index'));
        }

        return Redirect::route('backend.auth.login')->with('error', 'Invalid Credentials!')->withInput();
    }

    public function getLogout()
    {
        Auth::logout();
        return Redirect::intended(route('backend.auth.login'));
    }

}