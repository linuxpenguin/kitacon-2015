<?php

class BackendAccountController extends BaseController {

    protected $layout = 'backend.template';

    protected $breadcrumbs;

    public function __construct(Breadcrumb $breadcrumb)
    {
        $this->breadcrumbs = $breadcrumb;
        $this->breadcrumbs->addLink('Dashboard', route('backend.dashboard.index'));
        $this->breadcrumbs->addLink('My Account', route('backend.account.index'));
    }

    public function getIndex()
    {
        $objAccount = Auth::User();

        $this->layout->title = "Kitacon Committee - My Account";
        $this->layout->content = View::make('backend.account.index');
        $this->layout->arrBreadcrumbs = $this->breadcrumbs->getBreadCrumbs();
    }

}
