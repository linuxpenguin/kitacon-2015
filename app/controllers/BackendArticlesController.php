<?php

class BackendArticlesController extends BaseController
{

    protected $layout = 'backend.template';

    protected $breadcrumbs;

    public function __construct(Breadcrumb $breadcrumb)
    {
        $this->breadcrumbs = $breadcrumb;
        $this->breadcrumbs->addLink('Dashboard', route('backend.dashboard.index'));
        $this->breadcrumbs->addLink('News Articles', route('backend.articles.index'));
    }

    public function getIndex()
    {
        $this->layout->title = 'Kitacon Committee - Manage News Articles';
        $this->layout->content = View::make('backend.articles.index');

        // Pass News Articles
        $this->layout->content->objArticles = Article::all();
        $this->layout->arrBreadcrumbs = $this->breadcrumbs->getBreadCrumbs();
    }

    public function edit($id)
    {
        // Pass the article, or redirect back to articles/
        $objArticle = Article::find($id);

        if(!$objArticle)
        {
            return Redirect::route('backend.articles.index')
                ->with('error', 'Article does not exist!');
        }

        $this->layout->title = 'Kitacon Committee - Edit Article';
        $this->layout->content = View::make('backend.articles.edit')->with('objArticle', $objArticle);
        $this->breadcrumbs->addLink('Edit: ' . $objArticle->title, route('backend.articles.edit'));
        $this->layout->arrBreadcrumbs = $this->breadcrumbs->getBreadCrumbs();
    }

    public function update($id)
    {
        // Pass the article, or redirect back to articles/
        $objArticle = Article::find($id);

        if(!$objArticle)
        {
            return Redirect::to(route('backend.articles.index'))
                ->with('error', 'Unable to update, article does not exist!');
        }

        $rules = [
            '_token'                  => 'required',
            'title'                   => 'required|min:2',
            'published'               => 'required',
            'content'                 => 'required|min:18',
        ];

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails())
        {
            return Redirect::route('backend.articles.edit', ['id' => $id])
                ->withErrors($validator)
                ->withInput();
        }

        $objArticle->title      = Input::get('title');
        $objArticle->slug       = Str::slug(Input::get('title'));
        $objArticle->content    = Input::get('content');
        $objArticle->published  = (Input::get('published') == "on") ? 1 : 0;
        $objArticle->save();

        return Redirect::route('backend.articles.edit', ['id' => $id])
            ->with('success', 'Article Updated!');
    }

    public function create()
    {
        $this->layout->title = 'Kitacon Committee - Create Article';
        $this->layout->content = View::make('backend.articles.create');
        $this->breadcrumbs->addLink('Create Article', route('backend.articles.create'));
        $this->layout->arrBreadcrumbs = $this->breadcrumbs->getBreadCrumbs();
    }

    public function store()
    {

        $rules = [
            '_token'                  => 'required',
            'title'                   => 'required|min:2',
            'published'               => 'required',
            'content'                 => 'required|min:18',
        ];

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails())
        {
            return Redirect::route('backend.articles.create')
                ->withErrors($validator)
                ->withInput();
        }

        $objArticle = new Article();
        $objArticle->user_id    = Auth::User()->id;
        $objArticle->title      = Input::get('title');
        $objArticle->slug       = Str::slug(Input::get('title'));
        $objArticle->content    = Input::get('content');
        $objArticle->published  = (Input::get('published') == "on") ? 1 : 0;
        $objArticle->save();

        return Redirect::route('backend.articles.index')
            ->with('success', 'Article was created successfully!');
    }

    public function delete($id)
    {
        // Pass the article, or redirect back to articles/
        $objArticle = Article::find($id);

        if(!$objArticle)
        {
            return Redirect::route('backend.articles.index')
                ->with('error', 'Unable to delete, article does not exist!');
        }

        $objArticle->destroy($id);

        return Redirect::route('backend.articles.index')
            -> with('success', 'The requested article was deleted!');

    }
}
