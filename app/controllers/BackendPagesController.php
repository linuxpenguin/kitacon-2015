<?php

class BackendPagesController extends BaseController
{

    protected $layout = 'backend.template';

    protected $breadcrumbs;

    public function __construct(Breadcrumb $breadcrumb)
    {
        $this->breadcrumbs = $breadcrumb;
        $this->breadcrumbs->addLink('Dashboard', route('backend.dashboard.index'));
        $this->breadcrumbs->addLink('CMS Pages', route('backend.cms.index'));
    }

    public function getIndex()
    {
        $this->layout->title = 'Kitacon Committee - Manage CMS Pages';
        $this->layout->content = View::make('backend.cms.index');

        // Pass CMS Pages
        $this->layout->content->objPages = Page::all();
        $this->layout->arrBreadcrumbs = $this->breadcrumbs->getBreadCrumbs();
    }

    public function create()
    {
        $this->layout->title = 'Kitacon Committee - Create CMS Page';
        $this->layout->content = View::make('backend.cms.create');
        $this->breadcrumbs->addLink('Create CMS Page', route('backend.cms.create'));
        $this->layout->arrBreadcrumbs = $this->breadcrumbs->getBreadCrumbs();
    }

    public function store()
    {

        $rules = [
            '_token'                  => 'required',
            'title'                   => 'required|min:2',
            'live'                    => 'required',
            'content'                 => 'required|min:18',
        ];

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails())
        {
            return Redirect::route('backend.cms.create')
                ->withErrors($validator)
                ->withInput();
        }

        $objPage = new Page();
        $objPage->title      = Input::get('title');
        $objPage->slug       = Str::slug(Input::get('title'));
        $objPage->content    = Input::get('content');
        $objPage->live       = (Input::get('live') == "on") ? 1 : 0;
        $objPage->save();

        return Redirect::route('backend.cms.index')
            ->with('success', 'CMS Page was created successfully!');
    }

    public function edit($id)
    {
        $objPage = Page::find($id);

        if(!$objPage)
        {
            return Redirect::route('backend.cms.index')
                ->with('error', 'CMS Page does not exist!');
        }

        $this->layout->title = 'Kitacon Committee - Edit CMS Page';
        $this->layout->content = View::make('backend.cms.edit')->with('objPage', $objPage);
        $this->breadcrumbs->addLink('Edit: ' . $objPage->title, route('backend.cms.edit'));
        $this->layout->arrBreadcrumbs = $this->breadcrumbs->getBreadCrumbs();
    }

    public function update($id)
    {
        $objPage = Page::find($id);

        if(!$objPage)
        {
            return Redirect::to(route('backend.cms.index'))
                ->with('error', 'Unable to update, CMS Page does not exist!');
        }

        $rules = [
            '_token'                  => 'required',
            'title'                   => 'required|min:2',
            'live'                    => 'required',
            'content'                 => 'required|min:18',
        ];

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails())
        {
            return Redirect::route('backend.cms.edit', ['id' => $id])
                ->withErrors($validator)
                ->withInput();
        }

        $objPage->title      = Input::get('title');
        $objPage->slug       = Str::slug(Input::get('title'));
        $objPage->content    = Input::get('content');
        $objPage->live       = (Input::get('live') == "on") ? 1 : 0;
        $objPage->save();

        return Redirect::route('backend.cms.edit', ['id' => $id])
            ->with('success', 'CMS Page Updated!');
    }

    public function delete($id)
    {
        $objPage = Page::find($id);

        if(!$objPage)
        {
            return Redirect::route('backend.cms.index')
                ->with('error', 'Unable to delete, CMS Page does not exist!');
        }

        $objPage->destroy($id);

        return Redirect::route('backend.cms.index')
            -> with('success', 'The requested CMS Page was deleted!');
    }

}
