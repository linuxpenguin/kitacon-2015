<?php

class BackendUsersController extends BaseController {

    protected $layout = 'backend.template';

    public function getIndex()
    {
        $this->layout->title = "Kitacon Committee - Manage Users";
        $this->layout->content = View::make('backend.users.index');

        $this->layout->content->objUsers = User::paginate(15);

        // Build Breadcrumb
        $objBreadcrumbs = new Breadcrumb;
        $objBreadcrumbs->addLink('Dashboard', route('backend.dashboard.index'));
        $objBreadcrumbs->addLink('Manage Users', route('backend.users.index'));
        $this->layout->arrBreadcrumbs = $objBreadcrumbs->getBreadCrumbs();
    }

    public function create()
    {
        $this->layout->title = 'Kitacon Committee - Create Users';
        $this->layout->content = View::make('backend.users.create');

        // Build Breadcrumb
        $objBreadcrumbs = new Breadcrumb;
        $objBreadcrumbs->addLink('Dashboard', route('backend.dashboard.index'));
        $objBreadcrumbs->addLink('Manage Users', route('backend.users.index'));
        $objBreadcrumbs->addLink('Create Users', route('backend.users.create'));
        $this->layout->arrBreadcrumbs = $objBreadcrumbs->getBreadCrumbs();
    }

    public function store()
    {

        $rules = [
            '_token'                    => 'required',
            'email'                     => 'required|email',
            'password'                  => 'required|min:8|confirmed',
            'password_confirmation'     => 'required|min:8',
            'first_name'                => 'required|min:2',
            'last_name'                 => 'required|min:2',
        ];

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails())
        {
            return Redirect::route('backend.users.create')
                ->withErrors($validator)
                ->withInput();
        }

        $objUser = new User();
        $objUser->email         = Input::get('email');
        $objUser->first_name    = Input::get('first_name');
        $objUser->last_name     = Input::get('last_name');
        $objUser->admin         = (Input::get('admin') == "on") ? 1 : 0;
        $objUser->password  = Hash::make(Input::get('password'));
        $objUser->save();

        return Redirect::route('backend.users.index')
            ->with('success', 'User Account was created successfully!');
    }

    public function edit($id)
    {
        $objUser = User::find($id);

        if(!$objUser)
        {
            return Redirect::route('backend.users.index')
                ->with('error', 'User Account does not exist!');
        }

        $this->layout->title = 'Kitacon Committee - Edit User Account';
        $this->layout->content = View::make('backend.users.edit')->with('objUser', $objUser);

        // Build Breadcrumb
        $objBreadcrumbs = new Breadcrumb;
        $objBreadcrumbs->addLink('Dashboard', route('backend.dashboard.index'));
        $objBreadcrumbs->addLink('Manage Users', route('backend.users.index'));
        $objBreadcrumbs->addLink('Edit: ' . $objUser->email, route('backend.users.edit'));
        $this->layout->arrBreadcrumbs = $objBreadcrumbs->getBreadCrumbs();
    }

    public function update($id)
    {
        $objUser = User::find($id);

        if(!$objUser)
        {
            return Redirect::to(route('backend.users.index'))
                ->with('error', 'Unable to update, User Account does not exist!');
        }

        $rules = [
            '_token'                    => 'required',
            'password'                  => 'min:8|confirmed',
            'password_confirmation'     => 'min:8',
            'first_name'                => 'required|min:2',
            'last_name'                 => 'required|min:2',
        ];

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails())
        {
            return Redirect::route('backend.users.edit', ['id' => $id])
                ->withErrors($validator)
                ->withInput();
        }

        $objUser->first_name    = Input::get('first_name');
        $objUser->last_name     = Input::get('last_name');
        $objUser->admin         = (Input::get('admin') == "on") ? 1 : 0;

        if(Input::get('password'))
        {
            $objUser->password  = Hash::make(Input::get('password'));
        }

        $objUser->save();

        return Redirect::route('backend.users.edit', ['id' => $id])
            ->with('success', 'User Account Updated!');
    }

    public function delete($id)
    {
        $objUser = User::find($id);

        if(!$objUser)
        {
            return Redirect::route('backend.users.index')
                ->with('error', 'Unable to delete, User Account does not exist!');
        }

        $objUser->destroy($id);

        return Redirect::route('backend.users.index')
            -> with('success', 'The requested User Account was deleted!');
    }

}