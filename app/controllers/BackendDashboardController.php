<?php

class BackendDashboardController extends BaseController {

    protected $layout = 'backend.template';

    protected $breadcrumbs;

    public function __construct(Breadcrumb $breadcrumb)
    {
        $this->breadcrumbs = $breadcrumb;
        $this->breadcrumbs->addLink('Dashboard', route('backend.dashboard.index'));
    }

    public function getIndex()
    {
        $this->layout->title = 'Kitacon Committee - Dashboard';
        $this->layout->content = View::make('backend.dashboard.index');
        $this->layout->arrBreadcrumbs = $this->breadcrumbs->getBreadCrumbs();
    }

}
