@section('content')

<section class="container" role="main">

<!-- Grid row -->
<div class="row">

    <!-- Data block -->
    <article class="col-sm-12">
        <div class="data-block">
            <header>
                <h2><span class="elusive icon-search"></span> &nbsp; Search</h2>
            </header>
            <section>

                <h3>Search Results ({{{ $intResults }}})@if(Input::get('q')) - "{{{ Input::get('q') }}}" @endif</h3>

                <hr />

                @if(!isset($intResults) or $intResults <= 0)
                    <p>No search results found.</p>
                @else
                    @if($objArticles->count() > 0)
                        <h4>Articles ({{{ $objArticles->count() }}})</h4>
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th style="text-align: left !important;">Article Title</th>
                                    <th>Author</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($objArticles as $objArticle)
                                <tr>
                                    <td class="text-center">
                                        {{{ $objArticle->id }}}
                                    </td>
                                    <td>
                                        {{{ $objArticle->title }}}
                                    </td>
                                    <td class="text-center">
                                        {{{ $objArticle->user->getName() }}}
                                    </td>
                                    <td class="text-center">
                                        @if($objArticle->published)
                                        <div class="label label-success">
                                            Published
                                        </div>
                                        @else
                                        <div class="label label-warning">
                                            Un-published
                                        </div>
                                        @endif
                                    </td>
                                    <td class="toolbar text-center">
                                        <div class="btn-group">
                                            <a href="{{{ route('backend.articles.edit', ['id' => $objArticle->id]) }}}">Edit</a>
                                            -
                                            <a href="{{{ route('backend.articles.destroy', ['id' => $objArticle->id]) }}}">Delete</a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endif

                    @if($objPages->count() > 0)
                        <h4>CMS Pages ({{{ $objPages->count() }}})</h4>
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th style="text-align: left !important;">Page Title</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($objPages as $objPage)
                                <tr>
                                    <td class="text-center">
                                        {{{ $objPage->id }}}
                                    </td>
                                    <td>
                                        {{{ $objPage->title }}}
                                    </td>
                                    <td class="text-center">
                                        @if($objPage->live)
                                        <div class="label label-success">
                                            Live
                                        </div>
                                        @else
                                        <div class="label label-warning">
                                            Not Live
                                        </div>
                                        @endif
                                    </td>
                                    <td class="toolbar text-center">
                                        <div class="btn-group">
                                            <a href="{{{ route('backend.cms.edit', ['id' => $objPage->id]) }}}">Edit</a>
                                            -
                                            <a href="{{{ route('backend.cms.destroy', ['id' => $objPage->id]) }}}">Delete</a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endif

                    @if($objUsers->count() > 0)
                        <h4>User Accounts ({{{ $objUsers->count() }}})</h4>
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th style="text-align: left !important;">Email Address</th>
                                    <th>Full Name</th>
                                    <th>Privileges</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($objUsers as $objUser)
                                <tr>
                                    <td class="text-center">
                                        {{{ $objUser->id }}}
                                    </td>
                                    <td>
                                        <a href="{{{ route('backend.users.edit', ['id' => $objUser->id]) }}}">{{{ $objUser->email }}}</a>
                                    </td>
                                    <td class="text-center">
                                        {{{ $objUser->getName() }}}
                                    </td>
                                    <td class="text-center">
                                        @if($objUser->admin)
                                        <div class="label label-success">
                                            Administrator
                                        </div>
                                        @else
                                        <div class="label label-warning">
                                            Standard User
                                        </div>
                                        @endif
                                    </td>
                                    <td class="toolbar text-center">
                                        <div class="btn-group">
                                            <a href="{{{ route('backend.users.edit', ['id' => $objUser->id]) }}}">Edit</a>
                                            -
                                            <a href="{{{ route('backend.users.destroy', ['id' => $objUser->id]) }}}">Delete</a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endif
                @endif

            </section>
        </div>
    </article>
    <!-- /Data block -->

</div>
<!-- /Grid row -->

</section>

@stop