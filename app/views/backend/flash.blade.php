<div class="row">
    <div class="col-md-12">
        @if(Session::has('error'))
        <div class="alert alert-danger alert-dismissable fade in">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true" title="" data-original-title="Close alert">&times;</button>
            {{ Session::get('error') }}
        </div>
        @endif

        @if(Session::has('info'))
        <div class="alert alert-info alert-dismissable fade in">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ Session::get('info') }}
        </div>
        @endif

        @if(Session::has('warning'))
        <div class="alert alert-warning alert-dismissable fade in">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ Session::get('warning') }}
        </div>
        @endif

        @if(Session::has('success'))
        <div class="alert alert-success alert-dismissable fade in">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ Session::get('success') }}
        </div>
        @endif
    </div>
</div>