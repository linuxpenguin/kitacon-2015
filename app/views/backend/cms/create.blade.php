@section('content')

<section class="container" role="main">

    <!-- Grid row -->
    <div class="row">

        <!-- Data block -->
        <article class="col-sm-12">
            <div class="data-block">
                <header>
                    <h2><span class="elusive icon-th-list"></span> &nbsp; CMS Pages</h2>
                </header>
                <section>

                    <h3>Create CMS Page</h3>

                    @include('backend.flash')

                    {{ Form::open(['class' => 'form-horizontal login-form']) }}

                        @include('backend.cms._cms_page_form')

                    {{ Form::close() }}

                </section>
            </div>
        </article>

    </div>
</section>


@stop