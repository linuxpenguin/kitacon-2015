@section('content')

<section class="container" role="main">

    <!-- Grid row -->
    <div class="row">

        <!-- Data block -->
        <article class="col-sm-12">
            <div class="data-block">
                <header>
                    <h2><span class="elusive icon-th-list"></span> &nbsp; News Articles</h2>
                </header>
                <section>

                    <h3>Edit CMS Page</h3>

                    @include('backend.flash')

                    {{ Form::model($objPage, ['route' => ['backend.cms.update', $objPage->id], 'class' => 'form-horizontal login-form', 'method' => 'put']) }}

                        @include('backend.cms._cms_page_form')

                    {{ Form::close() }}

                </section>
            </div>
        </article>

    </div>
</section>


@stop