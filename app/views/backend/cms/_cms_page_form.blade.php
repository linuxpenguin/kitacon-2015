<div class="form-group">
    <label for="title" class="col-sm-2 control-label">Page Title</label>
    <div class="col-sm-5">
        {{ Form::input('text', 'title', (isset($objPage) ? $objPage->title : null), array('class' => 'form-control')) }}
        <div class="help-block">
            <span class="text-danger">{{{ $errors->first('title') }}}</span>
        </div>
    </div>
</div>

<div class="form-group">
    <label for="title" class="col-sm-2 control-label">Live</label>
    <div class="col-sm-5">
        <div class="checkbox styled-checkbox">
            <label>
                {{ Form::checkbox('live', null, (isset($objPage) ? (($objPage->live == 1) ? true : false) : null), ['style' => 'display: none;']) }}
            </label>
        </div>
    </div>
</div>

<div class="form-group">
    <label for="title" class="col-sm-2 control-label">Page Content</label>
    <div class="col-sm-8">
        {{ Form::textarea('content', (isset($objPage) ? $objPage->content : null), array('class' => 'wysiwyg form-control')) }}
        <div class="help-block">
            <span class="text-danger">{{{ $errors->first('content') }}}</span>
        </div>
    </div>
</div>

<div class="form-group">
    <div class="col-md-2 col-md-offset-2">
        @if(isset($objPage))
            {{ Form::submit('Save Changes', array('class' => 'btn btn-default btn-lg btn-block')) }}
        @else
            {{ Form::submit('Create CMS Page', array('class' => 'btn btn-default btn-lg btn-block')) }}
        @endif
    </div>
</div>