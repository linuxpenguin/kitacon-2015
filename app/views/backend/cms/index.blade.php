@section('content')

<section class="container" role="main">

<!-- Grid row -->
<div class="row">

    <!-- Data block -->
    <article class="col-sm-12">
        <div class="data-block">
            <header>
                <h2><span class="elusive icon-th-list"></span> &nbsp; CMS Pages</h2>
                <a href="{{{ route('backend.cms.create') }}}" class="btn btn-default">Create Page</a>
            </header>
            <section>

                <h3>Manage CMS Pages</h3>
                <p>Below are all the cms pages, to manage them click the associated options next to the relative page.</p>

                @include('backend.flash')

                <!-- Basic table with hovers -->
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th class="text-left">Page Title</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($objPages as $objPage)
                        <tr>
                            <td class="text-center">
                                {{{ $objPage->id }}}
                            </td>
                            <td>
                                {{{ $objPage->title }}}
                            </td>
                            <td class="text-center">
                                @if($objPage->live)
                                    <div class="label label-success">
                                        Live
                                    </div>
                                @else
                                    <div class="label label-warning">
                                        Not Live
                                    </div>
                                @endif
                            </td>
                            <td class="toolbar text-center">
                                <div class="btn-group">
                                    <a href="{{{ route('backend.cms.edit', ['id' => $objPage->id]) }}}">Edit</a>
                                     -
                                    <a href="{{{ route('backend.cms.destroy', ['id' => $objPage->id]) }}}">Delete</a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /Basic table with hovers -->

            </section>
        </div>
    </article>
    <!-- /Data block -->

</div>
<!-- /Grid row -->

</section>

@stop