<!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <title>{{{ $title or 'Kitacon Committee' }}}</title>
            <meta name="viewport" content="width=device-width, initial-scale=1.0">

            <!-- Styles -->
            <link rel="stylesheet" href="/css/sangoma-blue.css">

            <!-- PrettyCheckable Styles -->
            <link rel='stylesheet' type='text/css' href='/css/plugins/prettycheckable/prettyCheckable.css'>

            <!-- JS Libs -->
            <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
            <script>window.jQuery || document.write('<script src="js/libs/jquery.js"><\/script>')</script>
            <script src="/js/libs/modernizr.js"></script>

            <!-- IE8 support of media queries and CSS 2/3 selectors -->
            <!--[if lt IE 9]>
            <script src="/js/libs/respond.min.js"></script>
            <script src="/js/libs/selectivizr.js"></script>
            <![endif]-->
        </head>
    <body @if(!Auth::Check())class="login"@endif>

        <div id="wrapper">

            <section class="container" role="main">

                @if(Auth::Check())
                    @include('backend.navigation')

                    @include('backend.breadcrumb')
                @endif

                @yield('content')

            </section>

        </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

        <!-- Latest compiled and minified JavaScript -->
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>

        <!-- Wysihtml5 -->
        <script src="/js/plugins/wysihtml5/wysihtml5-0.3.0.js"></script>
        <script src="/js/plugins/wysihtml5/bootstrap-wysihtml5.js"></script>

        <!-- PrettyCheckable checkbox and radio -->
        <script src="/js/plugins/prettyCheckable/prettyCheckable.js"></script>

        <!-- On Load -->
        <script>
            $(document).ready(function() {
                $('.wysiwyg').wysihtml5();
                $('.styled-checkbox input, .styled-radio input').prettyCheckable();
            });
        </script>


        </body>
</html>