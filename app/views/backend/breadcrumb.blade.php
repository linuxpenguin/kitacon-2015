<section class="container" role="main">
    <ul class="breadcrumb">
        @foreach ($arrBreadcrumbs['breadcrumbs'] as $arrBreadcrumb)
        <li class="{{ $arrBreadcrumb['active'] or '' }}">
            @if (isset($arrBreadcrumb['link']))
            <a href="{{ $arrBreadcrumb['link'] }}">
                {{ $arrBreadcrumb['name'] }}
            </a>
            @else
            {{ $arrBreadcrumb['name'] }}
            @endif
        </li>
        @endforeach

    </ul>
</section>