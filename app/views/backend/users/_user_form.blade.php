<div class="form-group">
    <label for="title" class="col-sm-2 control-label">Email Address</label>
    <div class="col-sm-5">
        @if(isset($objUser))
            {{ Form::input('email', 'email', $objUser->email, array('class' => 'form-control', 'disabled')) }}
        @else
            {{ Form::input('email', 'email', null, array('class' => 'form-control')) }}
        @endif
        <div class="help-block">
            <span class="text-danger">{{{ $errors->first('email') }}}</span>
        </div>
    </div>
</div>

<hr />

<div class="form-group">
    <label for="title" class="col-sm-2 control-label">Password</label>
    <div class="col-sm-5">
        {{ Form::input('password', 'password', null, array('class' => 'form-control')) }}
        <div class="help-block">
            <span class="text-danger">{{{ $errors->first('password') }}}</span>
        </div>
    </div>
</div>

<div class="form-group">
    <label for="title" class="col-sm-2 control-label">Confirm Password</label>
    <div class="col-sm-5">
        {{ Form::input('password', 'password_confirmation', null, array('class' => 'form-control')) }}
        <div class="help-block">
            <span class="text-danger">{{{ $errors->first('password_confirmation') }}}</span>
        </div>
    </div>
</div>

<hr />

<div class="form-group">
    <label for="title" class="col-sm-2 control-label">First Name</label>
    <div class="col-sm-5">
        {{ Form::input('text', 'first_name', (isset($objUser) ? $objUser->first_name : null), array('class' => 'form-control')) }}
        <div class="help-block">
            <span class="text-danger">{{{ $errors->first('first_name') }}}</span>
        </div>
    </div>
</div>

<div class="form-group">
    <label for="title" class="col-sm-2 control-label">Last Name</label>
    <div class="col-sm-5">
        {{ Form::input('text', 'last_name', (isset($objUser) ? $objUser->last_name : null), array('class' => 'form-control')) }}
        <div class="help-block">
            <span class="text-danger">{{{ $errors->first('last_name') }}}</span>
        </div>
    </div>
</div>

<hr />

<div class="form-group">
    <label for="title" class="col-sm-2 control-label">Committee Admin</label>
    <div class="col-sm-5">
        <div class="checkbox styled-checkbox">
            <label>
                {{ Form::checkbox('admin', null, (isset($objUser) ? (($objUser->admin == 1) ? true : false) : null), ['style' => 'display: none;']) }}
            </label>
        </div>
    </div>
</div>

<div class="form-group">
    <div class="col-md-2 col-md-offset-2">
        @if(isset($objUser))
            {{ Form::submit('Save Changes', array('class' => 'btn btn-default btn-lg btn-block')) }}
        @else
            {{ Form::submit('Create Account', array('class' => 'btn btn-default btn-lg btn-block')) }}
        @endif
    </div>
</div>