@section('content')

<section class="container" role="main">

<!-- Grid row -->
<div class="row">

    <!-- Data block -->
    <article class="col-sm-12">
        <div class="data-block">
            <header>
                <h2><span class="elusive icon-user"></span> &nbsp; Manage Users</h2>
                <ul class="data-header-actions">
                    <li><a href="{{{ route('backend.users.create') }}}">Create User Account</a></li>
                    <li>
                        <form class="header-search">
                            <input type="text" class="form-control" placeholder="Search..." />
                        </form>
                    </li>
                </ul>
            </header>
            <section>

                <h3>Manage User Accounts</h3>
                <p>Below are all the user accounts, to manage them click the associated options next to the relative page.</p>

                @include('backend.flash')

                <!-- Basic table with hovers -->
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th style="text-align: left !important;">Email Address</th>
                            <th>Full Name</th>
                            <th>Privileges</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($objUsers as $objUser)
                        <tr>
                            <td class="text-center">
                                {{{ $objUser->id }}}
                            </td>
                            <td>
                                <a href="{{{ route('backend.users.edit', ['id' => $objUser->id]) }}}">{{{ $objUser->email }}}</a>
                            </td>
                            <td class="text-center">
                                {{{ $objUser->getName() }}}
                            </td>
                            <td class="text-center">
                                @if($objUser->admin)
                                <div class="label label-success">
                                    Administrator
                                </div>
                                @else
                                <div class="label label-warning">
                                    Standard User
                                </div>
                                @endif
                            </td>
                            <td class="toolbar text-center">
                                <div class="btn-group">
                                    <a href="{{{ route('backend.users.edit', ['id' => $objUser->id]) }}}">Edit</a>
                                     -
                                    <a href="{{{ route('backend.users.destroy', ['id' => $objUser->id]) }}}">Delete</a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>

                    <!--- Pagination -->
                    {{ $objUsers->links() }}
                    <!--- /Pagination -->

                </div>
                <!-- /Basic table with hovers -->

            </section>
        </div>
    </article>
    <!-- /Data block -->

</div>
<!-- /Grid row -->

</section>

@stop