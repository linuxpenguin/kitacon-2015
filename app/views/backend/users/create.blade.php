@section('content')

<section class="container" role="main">

    <!-- Grid row -->
    <div class="row">

        <!-- Data block -->
        <article class="col-sm-12">
            <div class="data-block">
                <header>
                    <h2><span class="elusive icon-user"></span> &nbsp; Manage Users</h2>
                </header>
                <section>

                    <h3>Create User Account</h3>

                    @include('backend.flash')

                    {{ Form::open(['class' => 'form-horizontal login-form']) }}

                        @include('backend.users._user_form')

                    {{ Form::close() }}

                </section>
            </div>
        </article>

    </div>
</section>


@stop