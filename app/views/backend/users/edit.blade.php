@section('content')

<section class="container" role="main">

    <!-- Grid row -->
    <div class="row">

        <!-- Data block -->
        <article class="col-sm-12">
            <div class="data-block">
                <header>
                    <h2><span class="elusive icon-user"></span> &nbsp; Manage Users</h2>
                </header>
                <section>

                    <h3>Edit User Account</h3>

                    @include('backend.flash')

                    {{ Form::model($objUser, ['route' => ['backend.users.update', $objUser->id], 'class' => 'form-horizontal login-form', 'method' => 'put']) }}

                        @include('backend.users._user_form')

                    {{ Form::close() }}

                </section>
            </div>
        </article>

    </div>
</section>


@stop