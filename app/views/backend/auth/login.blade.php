@section('content')

<!-- Login header -->
<div class="login-logo">
    <h1>Kitacon Committee</h1>
</div>
<!-- /Login header -->

<!-- Login form -->
{{ Form::open(array('class' => 'form-horizontal login-form')) }}

@include('backend.flash')

<div class="form-group">
    <div class="input-group">
        <span class="input-group-addon"><span class="elusive icon-user"></span></span>
        {{ Form::input('email', 'email', null, array('class' => 'form-control', 'placeholder' => 'john.smith@kitacon.org')) }}
    </div>
</div>

<div class="form-group">
    <div class="input-group">
        <span class="input-group-addon"><span class="elusive icon-key"></span></span>
        {{ Form::input('password', 'password', null, array('class' => 'form-control', 'placeholder' => 'Password')) }}
    </div>
</div>

{{ Form::submit('Secure Login', array('class' => 'btn btn-primary btn-lg btn-block')) }}

{{ Form::close() }}
<!-- /Login form -->

@stop