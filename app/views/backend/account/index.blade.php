@section('content')

<section class="container" role="main">

    <div class="row">

        <!-- Data block -->
        <!-- Data block -->
        <article class="col-sm-12">
            <div class="data-block">
                <header>
                    <h2>User Profile</h2>
                </header>
                <section>
                    <div class="row">
                        <div class="col-sm-6">
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Your Name</label>
                                    <div class="col-sm-5">
                                        <p class="form-control-static">{{{ Auth::User()->getName() }}}</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Profile avatar</label>
                                    <div class="col-sm-9">
                                        <div class="fileupload fileupload-new" data-provides="fileupload"><input type="hidden">
                                            <div class="fileupload-preview fileupload-small thumbnail" style="line-height: 50px;"><img src="img/sample_content/upload-50x50.png"></div>
                                            <div>
                                                                <span class="btn btn-default btn-file">
                                                                    <span class="fileupload-new">Select image</span>
                                                                    <span class="fileupload-exists">Change</span>
                                                                    <input type="file">
                                                                </span>
                                                <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload">Remove</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-sm-6">
                            <h4>About Me</h4>
                            <p>This will be the about me that will show on the frontend of the website.</p>
                        </div>
                    </div>
                </section>
            </div>
        </article>
        <!-- /Data block -->

    </div>

</section>

@stop