@section('content')

<section class="container" role="main">

    <!-- Grid row -->
    <div class="row">

        <!-- Data block -->
        <article class="col-sm-12">
            <div class="data-block">
                <header>
                    <h2><span class="elusive icon-th-list"></span> &nbsp; News Articles</h2>
                </header>
                <section>

                    <h3>Edit Article</h3>

                    @include('backend.flash')

                    {{ Form::model($objArticle, ['route' => ['backend.articles.update', $objArticle->id], 'class' => 'form-horizontal login-form', 'method' => 'put']) }}

                        @include('backend.articles._article_form')

                    {{ Form::close() }}

                </section>
            </div>
        </article>

    </div>
</section>


@stop