@section('content')

<section class="container" role="main">

<!-- Grid row -->
<div class="row">

    <!-- Data block -->
    <article class="col-sm-12">
        <div class="data-block">
            <header>
                <h2><span class="elusive icon-th-list"></span> &nbsp; News Articles</h2>
                <a href="{{{ route('backend.articles.create') }}}" class="btn btn-default">Create Article</a>
            </header>
            <section>

                <h3>Manage News Articles</h3>
                <p>Below are all the articles either published or not published, to manage them click the associated options next to the relative article.</p>

                @include('backend.flash')

                <!-- Basic table with hovers -->
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th class="text-left">Article Title</th>
                            <th>Author</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($objArticles as $objArticle)
                        <tr>
                            <td class="text-center">
                                {{{ $objArticle->id }}}
                            </td>
                            <td>
                                {{{ $objArticle->title }}}
                            </td>
                            <td class="text-center">
                                {{{ $objArticle->user->getName() }}}
                            </td>
                            <td class="text-center">
                                @if($objArticle->published)
                                    <div class="label label-success">
                                        Published
                                    </div>
                                @else
                                    <div class="label label-warning">
                                        Un-published
                                    </div>
                                @endif
                            </td>
                            <td class="toolbar text-center">
                                <div class="btn-group">
                                    <a href="{{{ route('backend.articles.edit', ['id' => $objArticle->id]) }}}">Edit</a>
                                     -
                                    <a href="{{{ route('backend.articles.destroy', ['id' => $objArticle->id]) }}}">Delete</a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /Basic table with hovers -->

            </section>
        </div>
    </article>
    <!-- /Data block -->

</div>
<!-- /Grid row -->

</section>

@stop