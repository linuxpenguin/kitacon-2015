<div class="form-group">
    <label for="title" class="col-sm-2 control-label">Article Title</label>
    <div class="col-sm-5">
        {{ Form::input('text', 'title', (isset($objArticle) ? $objArticle->title : null), array('class' => 'form-control')) }}
        <div class="help-block">
            <span class="text-danger">{{{ $errors->first('title') }}}</span>
        </div>
    </div>
</div>

<div class="form-group">
    <label for="title" class="col-sm-2 control-label">Published</label>
    <div class="col-sm-5">
        <div class="checkbox styled-checkbox">
            <label>
                {{ Form::checkbox('published', null, (isset($objArticle) ? (($objArticle->published == 1) ? true : false) : null), ['style' => 'display: none;']) }}
            </label>
        </div>
    </div>
</div>

<div class="form-group">
    <label for="title" class="col-sm-2 control-label">Article Content</label>
    <div class="col-sm-8">
        {{ Form::textarea('content', (isset($objArticle) ? $objArticle->content : null), array('class' => 'wysiwyg form-control')) }}
        <div class="help-block">
            <span class="text-danger">{{{ $errors->first('content') }}}</span>
        </div>
    </div>
</div>

<div class="form-group">
    <div class="col-md-2 col-md-offset-2">
        @if(isset($objArticle))
            {{ Form::submit('Save Changes', array('class' => 'btn btn-default btn-lg btn-block')) }}
        @else
            {{ Form::submit('Create Article', array('class' => 'btn btn-default btn-lg btn-block')) }}
        @endif
    </div>
</div>