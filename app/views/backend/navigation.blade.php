<!-- Main page header -->
<header id="header" class="container">

    <h1>Kitacon Committee</h1>

    <!-- User profile -->
    <div class="user-profile">
        <figure>

            <!-- User profile avatar -->
            <img alt="John Pixel avatar" src="http://placekitten.com/60/60">

            <!-- User profile info -->
            <figcaption>
                    <strong><a href="{{{ route('backend.account.index') }}}">{{{ Auth::User()->getName() }}}</a></strong>
                <ul>
                    <li><a href="{{{ route('backend.account.index') }}}" title="Profile">profile</a></li>
                    <li><a href="{{{ route('backend.account.index') }}}" title="Account settings">settings</a></li>
                    <li><a href="{{{ route('backend.auth.logout') }}}" title="Logout">logout</a></li>
                </ul>
            </figcaption>
            <!-- /User profile info -->

        </figure>
    </div>
    <!-- /User profile -->

    <!-- Main navigation -->
    <nav class="main-navigation navbar navbar-default" role="navigation">

        <!-- Collapse navigation for mobile -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".main-navigation-collapse">
                <span class="elusive icon-home"></span>  Dashboard
            </button>
        </div>
        <!-- /Collapse navigation for mobile -->

        <!-- Navigation -->
        <div class="main-navigation-collapse collapse navbar-collapse">

            <!-- Navigation items -->
            <ul class="nav navbar-nav">

                <!-- Active navigation items -->
                <li{{ Request::is('backend') ? ' class="active"' : null }}>
                    <a href="{{{ route('backend.dashboard.index') }}}"><span class="elusive icon-home"></span> Dashboard</a>
                </li>
                <!-- /Active navigation items -->

                <!-- Dropdown navigation items -->
                <li class="dropdown{{ Request::is('backend/articles*') ? ' active' : null }}{{ Request::is('backend/cms/*') ? ' active' : null }}">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="elusive icon-th"></span> Content Management <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{{ route('backend.articles.index') }}}"><span class="elusive icon-th-list"></span> News Articles</a></li>
                        <li><a href="{{{ route('backend.cms.index') }}}"><span class="elusive icon-align-left"></span> CMS Pages</a></li>
                        <li><a href="{{{ route('backend.cms.index') }}}"><span class="elusive icon-th-large"></span> Navigation Manager</a></li>
                    </ul>
                </li>
                <!-- /Dropdown navigation items -->

                <li class="{{ Request::is('backend/users*') ? ' active' : null }}">
                    <a href="{{{ route('backend.users.index') }}}"><span class="elusive icon-user"></span> Manage Users</a>
                </li>
            </ul>
            <!-- /Navigation items -->

            <!-- Navigation form -->
            {{ Form::open(['route' => 'backend.search.index', 'method' => 'get', 'class' => 'navbar-form navbar-right', 'role' => 'search']) }}
                <div class="form-group">
                    <input name="q" type="text" class="form-control" placeholder="Type here to search&hellip;">
                </div>
                <button type="submit" class="btn btn-default"><span class="elusive icon-search"></span></button>
            {{ Form::close() }}
            <!-- /Navigation form -->

        </div>
        <!-- /Navigation -->

    </nav>
    <!-- /Main navigation -->

</header>
<!-- /Main page header -->