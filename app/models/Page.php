<?php

class Page extends Eloquent {

    protected $softDelete = true;

    protected $table = 'cms_pages';

}