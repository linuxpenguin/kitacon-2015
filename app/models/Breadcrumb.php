<?php

class Breadcrumb
{
    protected $breadcrumbs;

    public function __construct()
    {
        $this->breadcrumbs = [];
    }

    public function addLink($name, $link)
    {
        $this->breadcrumbs[] = ['name' => $name, 'link' => $link];
    }

    public function getBreadcrumbs()
    {
        $breadcrumbs = $this->breadcrumbs;

        $last_link = array_pop($breadcrumbs);
        $last_link['active'] = true;
        unset($last_link['link']);

        $breadcrumbs[] = $last_link;

        return ['breadcrumbs' => $breadcrumbs];

    }
}